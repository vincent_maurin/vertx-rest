package com.glispa.vertx3;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author vincent.maurin
 */
public class ChapterMember {

    private static final AtomicInteger COUNTER = new AtomicInteger();

    private final int id;

    private String name;

    private String email;

    public ChapterMember() {
        this.id = COUNTER.getAndIncrement();
    }

    public ChapterMember(String name, String email) {
        this();
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
