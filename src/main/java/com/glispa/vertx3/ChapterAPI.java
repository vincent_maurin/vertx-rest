package com.glispa.vertx3;

import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * @author vincent.maurin
 */
public class ChapterAPI {
    private final Map<Integer, ChapterMember> members;

    public ChapterAPI(Router router) {
        this.members = new HashMap<>();
        // Data example set
        createMember("John Doe", "john.doe@domain.com");
        createMember("Jane Doe", "jane.doe@domain.com");

        router.route().handler(BodyHandler.create());

        router.get("/members").produces("application/json").handler(this::getAll);
        router.get("/members/:id").produces("application/json").handler(this::getOne);
        // This one is blocking for example
        router.post("/members").consumes("*/json").produces("application/json").blockingHandler(this::addOne);
    }

    private ChapterMember createMember(String name, String email) {
        ChapterMember member = new ChapterMember(name, email);
        members.put(member.getId(), member);
        return member;
    }

    public void getAll(RoutingContext routingContext) {
        routingContext.response().end(Json.encodePrettily(members.values()));
    }

    public void getOne(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        ChapterMember member = members.get(Integer.valueOf(id));
        if (member != null) {
            routingContext.response().end(Json.encodePrettily(member));
        } else {
            routingContext.response().setStatusCode(404).end();
        }
    }

    public void addOne(RoutingContext routingContext) {
        ChapterMember member = Json.decodeValue(routingContext.getBodyAsString(), ChapterMember.class);
        members.put(member.getId(), member);
        routingContext.response()
                .setStatusCode(201)
                .end(Json.encodePrettily(member));
    }
}
