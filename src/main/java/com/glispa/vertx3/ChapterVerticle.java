package com.glispa.vertx3;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author vincent.maurin
 */
public class ChapterVerticle extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(ChapterVerticle.class);


    @Override
    public void start(Future<Void> startFuture) throws Exception {

        // Create a router object.
        Router router = Router.router(vertx);

        // Static resources
        router.route("/assets/*").handler(StaticHandler.create("assets"));

        // Bind an API on a sub route
        Router apiRouter = Router.router(vertx);

        // CORS
        apiRouter.route("/api").handler(CorsHandler.create("*")
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.POST));

        ChapterAPI api = new ChapterAPI(apiRouter);

        router.mountSubRouter("/api", apiRouter);


        // Bind "/" to our hello message - so we are still compatible.
        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response.end("Hello");
        });

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(3000,
                        result -> {
                            if (result.succeeded()) {
                                LOG.info("Http server listening on http://localhost:3000/");
                                startFuture.complete();
                            } else {
                                startFuture.fail(result.cause());
                            }
                        });

    }


}
