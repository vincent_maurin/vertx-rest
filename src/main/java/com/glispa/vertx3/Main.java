/**
 *
 */
package com.glispa.vertx3;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

/**
 * @author vincent.maurin
 */
public class Main {


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        VertxOptions options = new VertxOptions();
        Vertx vertx = Vertx.vertx(options);
        vertx.deployVerticle(new ChapterVerticle());
    }
}
